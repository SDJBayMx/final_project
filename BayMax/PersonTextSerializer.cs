﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BayMax
{
    /// <summary>
    /// Author: David Drozd
    /// This class saves the information in a file into text format
    /// </summary>
    class PersonTextSerializer : PersonSerializer
    {
        public PersonTextSerializer(): base(PersonFileFormat.TXT)
        {
        }
        


        public override void Load()
        {
            //access the list of files in the data directory
            string[] persFileList = Directory.GetFiles(_dataDirPath);

            //for each file, create an person object and ask it to load itself form the file content
            foreach (string filePath in persFileList)
            {
                using (StreamReader reader = new StreamReader(new FileStream(filePath, FileMode.Open)))
                {
                    string personType = reader.ReadLine();

                    //create the person
                    Person pers = new Person();

                    pers.Load(reader);

                    //add the loaded person to the list
                    ListOfPeople.Add(pers);
                }
            }
        }

       

        public override void Save()
        {
            //go through all the people in the person list and create a file for each person 
            foreach (Person pers in ListOfPeople)
            {
                string filePath = $"{_dataDirPath}/pers{pers.Name}.dat";
                //create a file stream with a stream reader to store data into the file
                using (StreamWriter writer = new StreamWriter(new FileStream(filePath, FileMode.Create)))
                {
                    writer.WriteLine(pers.GetType().ToString());
                    pers.Save(writer);
                }
            }
        }
    }
}
