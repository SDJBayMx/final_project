﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BayMax
{
    /// <summary>
    /// Author: David Drozd
    /// This class takes in the type of diabetes the person has and then later on converts it 
    /// into text format
    /// </summary>
    class Diabetes
    {
        public int none = 0, type1 = 1, type2 = 2;

        public int _type;

        /// <summary>
        /// Constructor that accepts type to return Diabetes type
        /// </summary>
        /// <param name="type"></param>
        public Diabetes(int type)
        {
            _type = type;
        }
    }
}
