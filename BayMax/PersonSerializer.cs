﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace BayMax
{
    /// <summary>
    /// Author: David Drozd
    /// This class saves the information in a file
    /// </summary>
    public enum PersonFileFormat
    {
        TXT
    }

    public class PersonSerializer
    {
        /// <summary>
        /// The list of people that need to be saved
        /// </summary>
        private List<Person> listOfPeople;
        /// <summary>
        /// The path to where the person file will be stored
        /// </summary>
        protected string _dataDirPath;
        private PersonFileFormat TXT;

        public PersonSerializer(PersonFileFormat fileFormat)
        {


            //Create the path to save the files in the given format
            _dataDirPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "PersonData", fileFormat.ToString());

            //Check if the directory exists, if not, create it
            if (Directory.Exists(_dataDirPath) == false)
            {
                Directory.CreateDirectory(_dataDirPath);
            }
        }


        public string DataDirPath
        {
            get { return _dataDirPath; }
        }

        internal List<Person> ListOfPeople { get {return listOfPeople; } set { listOfPeople = value; }}

        public virtual void Load()
        {
            throw new InvalidOperationException("Please use a concrete serializer to load data");
        }

        public virtual void Save()
        {
            throw new InvalidOperationException("Please use a concrete serializer to save data");
        }


    }
}
