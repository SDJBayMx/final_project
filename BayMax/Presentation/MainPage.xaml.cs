﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BayMax.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private PeopleProfiles _personProfile;
        public MainPage()
        {
            this.InitializeComponent();
            _personProfile = new PeopleProfiles();
            //When the page is initialized go to the profile set up page first
            _frmContent.Navigate(typeof(ProfileSetUp),_personProfile);
        }



        private void OnShowAll(object sender, RoutedEventArgs e)
        {
            //Navigate to the Person profile page to show all profiles
            _frmContent.Navigate(typeof(ProfileList),_personProfile);
        }

        private void OnGoToAdd(object sender, RoutedEventArgs e)
        {
            //go to the profile set up page to create a profile
            _frmContent.Navigate(typeof(ProfileSetUp),_personProfile);
        }

        private void OnShowProfile(object sender, RoutedEventArgs e)
        {
            //go to the viewing profile page to see your profile
            _frmContent.Navigate(typeof(ViewProfile), _personProfile);
        }
    }
}
