﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BayMax.Presentation
{
    /// <summary>
    /// An empty page that can be navigated to within a Frame.
    /// </summary>
    public sealed partial class ProfileList : Page
    {
        /// <summary>
        /// The personProfile object to access the list of the people
        /// </summary>
        private PeopleProfiles _personProfile;

        public ProfileList()
        {
            this.InitializeComponent();

            //The person will be obtained via navigation from the main page
            _personProfile = null;
        }

        private void DisplayPeople()
        {
            //go through all people in the person class and add to list
            _lstPeople.Items.Clear();
            foreach (Person pers in _personProfile.ListOfPeople)
            {
                _lstPeople.Items.Add(pers);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //initialize the person object being sent by the Main Page
            _personProfile = e.Parameter as PeopleProfiles;

            //display the persons in case the list was updated
            DisplayPeople();
        }

        private void OnPersonSelected(object sender, SelectionChangedEventArgs e)
        {
            Person selectedPerson = _lstPeople.SelectedItem as Person;

            //display the list of person details into the _lstDetails
            _lstDetails.Items.Clear();
            foreach (Person pers in _personProfile.ListOfPeople)
            {
                _lstDetails.Items.Add(pers);
            }
        }

    }
}
