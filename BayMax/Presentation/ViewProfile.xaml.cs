﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace BayMax.Presentation
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewProfile : Page
    {

        private PeopleProfiles _personProfile;

        public ViewProfile()
        {
            this.InitializeComponent();
            _personProfile = null;
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //The person profile object being passed from the previous page
            _personProfile = e.Parameter as PeopleProfiles;
            //Assigning each property of the person in the list 
            List<string> name = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.Name).ToList();
            List<int> diabetes = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.Type_diabetes).ToList();
            List<int> age = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.Age).ToList();
            List<string> zodiak = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.Zodiak).ToList();
            List<double> bmi = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.BmiTotal).ToList();
            List<int> gender = _personProfile.ListOfPeople.Select(_personProfile => _personProfile.TheGender).ToList();

            int diabetesCheck = diabetes[0];
            int genderCheck = gender[0];
            //check which type of diabetes the person has
            if (diabetesCheck == 1)
            {
                _txtDiabetes.Text = "You have Type 1 Diabetes";
            }
            else if (diabetesCheck == 2)
            {
                _txtDiabetes.Text = "You have Type 2 Diabetes";
            }
            else if (diabetesCheck == 0)
            {
                _txtDiabetes.Text = "You do not have Diabetes";
            }
            //check what the gender is and set it to that
            if(genderCheck == 0)
            {
                _txtGender.Text = "Male";
            }
            else if(genderCheck == 1)
            {
                _txtGender.Text = "Female";
            }

            //set the properties of the person
            _txtName.Text = name[0];
            _txtAge.Text = age[0].ToString();
            _txtZodiak.Text = zodiak[0].ToString();
            _txtBmi.Text = bmi[0].ToString("0.00");
        }

    }
}
