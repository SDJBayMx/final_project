﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BayMax
{
    /// <summary>
    /// Author: Salim Elewa
    /// This class creates a list of the people and added any new person to it.
    /// </summary>
    class PeopleProfiles
    {
        /// <summary>
        /// List of people's profiles
        /// </summary>
        private List<Person> _lstOfPeople;

        public PeopleProfiles()
        {
            ListOfPeople = new List<Person>();
        }


        /// <summary>
        /// Property to access the list of people
        /// </summary>
        public List<Person> ListOfPeople
        {
            get { return _lstOfPeople; }

            set { _lstOfPeople = value; }
        }

        /// <summary>
        /// Add the person to the list
        /// </summary>
        /// <param name="Person">Person to be added</param>
        public void AddProfile(Person Person)
        {
            ListOfPeople.Add(Person);
        }
    }
}
