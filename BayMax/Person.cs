﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Author: Salim Elewa
/// This class creates a person that has all properties(eg. name, height, weight, etc).
/// This class also has a method that calls the methods to calculate the sign and BMI of the person.
/// </summary>
namespace BayMax
{

    class Person
    {
        /// <summary>
        /// The name of the person.
        /// </summary>
        private string _name;

        /// <summary>
        /// The day the person is born in.
        /// </summary>
        private int _day;

        /// <summary>
        /// The month the person is born in.
        /// </summary>
        private int _month;

        /// <summary>
        /// The year the person was born in.
        /// </summary>
        private int _year;

        /// <summary>
        /// An Object of the BMI class.
        /// </summary>
        public BMI _bmi;

        /// <summary>
        /// The weight of the person.
        /// </summary>
        private double _weight;

        /// <summary>
        /// The height of the person.
        /// </summary>
        private double _height;

        /// <summary>
        /// An object of the Diabetes class.
        /// </summary>
        public Diabetes _diabetes;

        /// <summary>
        /// The type of diabetes the person has.
        /// </summary>
        private int _type_diabetes;

        /// <summary>
        /// The gender of the person.
        /// </summary>
        private int _TheGender;

        /// <summary>
        /// An object of the signs class.
        /// </summary>
        private Signs _sign;

        /// <summary>
        /// The age of the person.
        /// </summary>
        //private int _age;

        /// <summary>
        /// The Zodiak of the person.
        /// </summary>
        private string _zodiak;


        public Person()
        {
            //Creating an instance of the diabetes class
            _diabetes = new Diabetes(Type_diabetes);

            //Creating an instance of the BMI class.
            _bmi = new BMI(Weight, Height);

            //Creating an instance of the signs class
            _sign = new Signs(Day, Month, Year);

        }
        #region Properties


        public string Name
        {
            get { return _name; }

            set {  _name = value; }
        }


        public int Type_diabetes
        {
            get { return _type_diabetes; }

            set { _type_diabetes = value; }
        }

        public double Height
        {
            get { return _height; }

            set {  _height = value; }
        }

        public double Weight
        {
            get { return _weight; }

            set { _weight = value; }
        }

        public int Day
        {
            get { return _day; }

            set { _day = value; }
        }

        public int Month
        {
            get { return _month; }

            set { _month = value; }
        }

        public int Year
        {
            get { return _year; }

            set { _year = value; }
        }

        public int TheGender
        {
            get { return _TheGender; }

            set { _TheGender = value; }
        }

        public string Zodiak
        {
            get { return _sign.Zodiak; }

            set {  _zodiak = value; }
        }

        public int Age
        {
            get { return 2017 - Year; }

            set { Age = value; }
        }


        public double BmiTotal
        {
            get { return _bmi.PersonBmi; }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Calculating the BMI and the person's zodiac
        /// </summary>
        public void Calculate()
        {
            _bmi.CalculateBMI(Height, Weight);
            _sign.CalculateSign(Month);
        }

        /// <summary>
        /// Load the list of people
        /// </summary>
        /// <param name="reader"></param>
        public void Load(StreamReader reader)
        {
            _name = reader.ReadLine();
            _bmi.PersonBmi = double.Parse(reader.ReadLine());
            _weight = int.Parse(reader.ReadLine());
            _height = int.Parse(reader.ReadLine());
            _type_diabetes = int.Parse(reader.ReadLine());

        }


        /// <summary>
        /// Saving the person into a file
        /// </summary>
        /// <param name="writer"></param>
        public void Save(StreamWriter writer)
        {
            //save the account properties
            writer.WriteLine(_name);
            writer.WriteLine(_bmi.PersonBmi);
            writer.WriteLine(_weight);
            writer.WriteLine(_height);
            writer.WriteLine(_type_diabetes);
        }
        #endregion
    }
}
