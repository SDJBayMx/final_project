﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.SpeechSynthesis;
using Windows.Media.SpeechRecognition;
using Windows.UI.Xaml.Controls;
/// <summary>
/// Author: Jacob Hazzard
/// The BMI class takes in weight and height parameters from the person class
/// and calculates the BMI based on those inputs.
/// </summary>
namespace BayMax
{
    class BMI
    {

        /// <summary>
        /// The height of the user
        /// </summary>
        private double _height;

        /// <summary>
        /// The weight of the user
        /// </summary>
        private double _weight;

        /// <summary>
        /// The BMI of the user
        /// </summary>
        private double _personBmi;

        /// <summary>
        /// Constructor to pass through Weight and Height
        /// </summary>
        /// <param name="weight"></param>
        /// <param name="height"></param>
        public BMI(double weight, double height)
        {
            _weight = Weight;
            _height = Height;
        }

        /// <summary>
        /// Getters and Setters for the variables
        /// </summary>
        public double Height { get { return _height; } set { _height = value; } }
        public double Weight { get { return _weight; } set { _weight = value; } }
        public double PersonBmi { get { return _personBmi; } set { _personBmi = value; } }

        /// <summary>
        /// Calculates BMI using Weight and Height variables
        /// </summary>
        /// <returns></returns>
        public void CalculateBMI(double height, double weight)
        {
            //height = height * 12;
            weight = weight * 0.45;
            height = height * 0.34;
            PersonBmi = weight/(Math.Pow(height, 2));
        }
        
    }
}
