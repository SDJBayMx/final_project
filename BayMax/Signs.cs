﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.SpeechRecognition;
using Windows.Media.SpeechSynthesis;
/// <summary>
/// Author: Jacob Hazzard
/// The purpose of the Signs class is to take in the users dates and return the age
/// and zodiak based on the month entered
/// </summary>
namespace BayMax
{
    class Signs
    {

        /// <summary>
        /// Enum that contains the months of the year in order
        /// </summary>
        enum Months
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        /// <summary>
        /// Variable that stores day as int
        /// </summary>
        private int _day;

        /// <summary>
        /// Variable that stores Month as String
        /// </summary>
        private int _month;

        /// <summary>
        /// Variable that stores year as int
        /// </summary>
        private int _year;

        /// <summary>
        /// Variable that stores age as int
        /// </summary>
        private int _age;

        private string _zodiak;

        /// <summary>
        /// Constructor that takes the day(int), month(string), and year(int)
        /// </summary>
        /// <param name="day"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        public Signs(int day, int month, int year)
        {
            _day = Day;
            _month = Month;
            _year = Year;
        }

        /// <summary>
        /// Getters and setters for the variables
        /// </summary>
        public int Day { get { return _day; } set { _day = value; } }
        public int Month { get { return _month; } set { _month = value; } }
        public int Year { get { return _year; } set { _year = value; } }
        public int Age { get { return _age; } set { _age = value; } }
        public string Zodiak { get { return _zodiak; } set { _zodiak = value; } }

        /// <summary>
        /// Calculates the sign of the user by putting the enums into arrays and searching the month array
        /// for the same value as the Month variable. Once found, it searches the zodiak array at the same
        /// index.
        /// </summary>
        /// <returns></returns>
        public void CalculateSign(int month)
        {
            if (month == (int)Months.January)
            {
                Zodiak = "Aquarius";
            }
            else if (month == (int)Months.February)
            {
                Zodiak = "Pisces";
            }
            else if (month == (int)Months.March)
            {
                Zodiak = "Aries";
            }
            else if (month == (int)Months.April)
            {
                Zodiak = "Taurus";
            }
            else if (month == (int)Months.May)
            {
                Zodiak = "Gemini";
            }
            else if (month == (int)Months.June)
            {
                Zodiak = "Cancer";
            }
            else if (month == (int)Months.July)
            {
                Zodiak = "Leo";
            }
            else if (month == (int)Months.August)
            {
                Zodiak = "Virgo";
            }
            else if (month == (int)Months.September)
            {
                Zodiak = "Libra";
            }
            else if (month == (int)Months.October)
            {
                Zodiak = "Scorpio";
            }
            else if (month == (int)Months.November)
            {
                Zodiak = "Sagittarius";
            }
            else if (month == (int)Months.December)
            {
                Zodiak = "Capricorn";
            }
            else
            {
                Zodiak = null;
            }

        }
    }
}
